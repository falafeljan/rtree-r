rtree-r
=======

This package provides an interface to an R-Tree Java implementation. It relies on David Moten's [rtree Java implementation](https://github.com/davidmoten/rtree).


Setup
-----

Make sure you have a suitable version of [R](https://www.r-project.org/) installed (≥ 3.2.0) and `rJava` installed and working. Furthermore, `devtools` is currently required to install this package.

Clone this repository via Git and run `$ cd java && make all`, which will compile the R-Tree wrapper `rtree-r` is using. Then, open an R session and install via `document(path/to/package)`, `install(path/to/package)`. If everything worked fine, you can use it now with the standard `library` import (`library(rtree)`).


Help
----

Hit up `?rtree` to find general information to this package. To go in-depth with the function, try `?newTree` and `?searchTree`.