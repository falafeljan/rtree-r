import com.github.davidmoten.rtree.Entry;
import com.github.davidmoten.rtree.RTree;
import com.github.davidmoten.rtree.geometry.Point;
import com.github.davidmoten.rtree.geometry.Circle;
import com.github.davidmoten.rtree.geometry.Geometry;
import com.github.davidmoten.rtree.geometry.Geometries;

import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

import com.google.common.primitives.Ints;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RTreeWrapper {
  private RTree<Integer, Geometry> tree;

  public static void main(String[] args) {
    RTreeWrapper wrapper = new RTreeWrapper();
    
    int[] ids = {0, 1, 2, 3, 4, 5};
    double[] values = {
      6.57, -5.3,
      1.222, -0.1,
      2, 2,
      1, 1,
      1.5, 1.5,
      1.2, 1.2
    };

    wrapper.addVectorAsMatrix(ids, values);

    int[] results = wrapper.search(2, 2, 1);
    System.out.println("Epsilon search results:");

    for (int i = 0; i < results.length; i++) {
      System.out.format("\t%s\n", results[i]);
    }
  }

  public RTreeWrapper() {
    tree = RTree.create();
  }

  public void addItem(int id, double x, double y) {
    tree = tree.add(id, Geometries.point(x, y));
  }

  public void addVectorAsMatrix(int[] ids, double[] values) {
    if (ids.length*2 != values.length) {
      System.out.println("Error: ids and values length do not match");
    }

    for (int i = 0; i < ids.length; i++) {
      addItem(ids[i], values[2*i], values[2*i + 1]);
    }
  }

  public void addMatrix(int[] ids, double[][] values) {
    if (ids.length != values.length) {
      System.out.println("Error: ids and values length do not match");
    }

    for (int i = 0; i < ids.length; i++) {
      addItem(ids[i], values[i][0], values[i][1]);
    }
  }

  public int[] search(double x, double y, double eps) {
    Iterable<Entry<Integer, Geometry>> results =
      tree.search(Point.create(x, y), eps).toBlocking().toIterable();

    List<Integer> resultValues = getResultValues(results);
    int[] ids = Ints.toArray(resultValues);
      // resultValues.toArray(new Integer[resultValues.size()]);

    return ids;
  }

  private List<Integer> getResultValues(Iterable<Entry<Integer, Geometry>> results) {
    List<Integer> values = new ArrayList<Integer>();
    Iterator<Entry<Integer, Geometry>> it = results.iterator();

    while (it.hasNext()) {
      Entry<Integer, Geometry> entry = it.next();
      values.add(entry.value());
    }

    return values;
  }
}